<?php

namespace App\Http\Middleware;

use Closure,
Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            if(auth()->user()->isAdmin()) {
                return $next($request);
            }

            return redirect('/home')->with('error', 'You do not have authorization to view this page');

        }
        return redirect('/login')->with('error', 'You must log in first');



    }
}
